require 'test_helper'

class RelatedUserTest < ActiveSupport::TestCase
  setup do
    @popular = tweets(:popular).becomes(Alert)
    @relevant = tweets(:relevant).becomes(Alert)
    @irelevant = tweets(:irelevant).becomes(Alert)

    @joao = users(:joao)
    @maria = users(:maria)
    @pedro = users(:pedro)
  end

  test "create a related user record" do
    related_user = RelatedUser.new(first_user: @joao, second_user: @maria, score: 2.0)
    assert related_user.save
  end

  test "not create an equal related user record" do
    RelatedUser.create(first_user: @joao, second_user: @maria, score: 2.0)

    related_user = RelatedUser.create(first_user: @joao, second_user: @maria, score: 1.0)
    assert_not related_user.save
  end

  test "get most similar users" do
    RelatedUser.create(first_user: @joao, second_user: @maria, score: 2.0)
    RelatedUser.create(first_user: @joao, second_user: @pedro, score: 1.0)
    RelatedUser.create(first_user: @maria, second_user: @pedro, score: 3.0)

    assert_equal @maria, RelatedUser.get_most_similar_users(@joao).first.other(@joao)
    assert_equal @pedro, RelatedUser.get_most_similar_users(@joao).second.other(@joao)
    assert_equal @maria, RelatedUser.get_most_similar_users(@pedro).first.other(@pedro)
    assert_equal @joao, RelatedUser.get_most_similar_users(@pedro).second.other(@pedro)
    assert_equal @pedro, RelatedUser.get_most_similar_users(@maria).first.other(@maria)
    assert_equal @joao, RelatedUser.get_most_similar_users(@maria).second.other(@maria)
  end

  test "get no similar users if there are no related user record" do
    assert RelatedUser.get_most_similar_users(@joao).blank?
    assert RelatedUser.get_most_similar_users(@pedro).blank?
    assert RelatedUser.get_most_similar_users(@maria).blank?
  end


end
