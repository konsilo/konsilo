require 'test_helper'

class CollaborativeFilteringScoreTest < ActiveSupport::TestCase

  test "create a collaborative filtering score for user" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)

    joao.collaborative_filtering_scores.create(:alert => popular, :score => 1.0)

    assert_equal popular, joao.collaborative_filtering_scores.first.alert
    assert_equal 1.0, joao.collaborative_filtering_scores.first.score
    assert_equal 1, joao.collaborative_filtering_scores.count
    assert_equal 1, popular.collaborative_filtering_scores.count
    assert_equal 1, CollaborativeFilteringScore.count
  end

  test "not allow the creation of an collaborative filtering score for existing user-alert relation" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)

    joao.collaborative_filtering_scores.create(:alert => popular, :score => 1.0)
    joao.collaborative_filtering_scores.create(:alert => popular, :score => 1.0)

    assert_equal 1, joao.collaborative_filtering_scores.count
    assert_equal 1, popular.collaborative_filtering_scores.count
    assert_equal 1, CollaborativeFilteringScore.count

    assert_raises(ActiveRecord::RecordInvalid){ joao.collaborative_filtering_scores.create!(:alert => popular, :score => 1.0) }
  end
end
