require 'test_helper'

class AlertTest < ActiveSupport::TestCase
  setup do
    @popular = tweets(:popular).becomes(Alert)
    @relevant = tweets(:relevant).becomes(Alert)
    @irelevant = tweets(:irelevant).becomes(Alert)

    @user = users(:joao)
  end

  test "save a new Alert" do
    alert = Alert.new
    assert alert.save
  end

  test "save a new Alert with all attributes" do
    alert = Alert.new(
      impact: "High",
      source: "#",
      title: "Cyber Attack",
      body: "A new cyber attack.",
      group_id: "001",
      number_of_users: 110,
      number_of_days: 3,
      burst_number: 110,
      first_date: Time.zone.parse("2015/Oct/27").utc,
      last_date: Time.zone.parse("2015/Oct/30").utc,
      tweet_type: 2,
      total_number: 112
    )

    assert alert.save
  end

  test "a new alert has no category by default" do
    assert @popular.category_list.empty?
    assert @relevant.category_list.empty?
    assert @irelevant.category_list.empty?
  end

  test "add new category to alert" do
    @popular.category_list.add("linux", "dos")

    @popular.save

    assert_not @popular.category_list.empty?
    assert @popular.category_list.include?("dos")
  end

  test "get the most and least popular categories" do
    @popular.category_list.add("linux", "dos")
    @popular.save
    @relevant.category_list.add("dos")
    @relevant.save

    assert_equal "dos", ActsAsTaggableOn::Tag.most_used.first.name
    assert_equal "linux", ActsAsTaggableOn::Tag.least_used.first.name
  end

  test "get alerts categorized as Linux and Dos" do
    @popular.category_list.add("linux", "dos")
    @popular.save
    @relevant.category_list.add("dos")
    @relevant.save
    @irelevant.category_list.add("dos", "linux")
    @irelevant.save

    assert Alert.categorized_as_all(["dos", "linux"]).include? @popular
    assert Alert.categorized_as_all(["dos", "linux"]).include? @irelevant
    assert_not Alert.categorized_as_all(["dos", "linux"]).include? @relevant
  end

  test "get alerts categorized as Linux OR Dos" do
    @popular.category_list.add("linux", "dos")
    @popular.save
    @relevant.category_list.add("dos")
    @relevant.save
    @irelevant.category_list.add("linux")
    @irelevant.save

    assert Alert.categorized_as_at_least_one(["dos", "linux"]).include? @popular
    assert Alert.categorized_as_at_least_one(["dos", "linux"]).include? @irelevant
    assert Alert.categorized_as_at_least_one(["dos", "linux"]).include? @relevant
  end

  test "get alerts not categorized as neither Linux or Dos" do
    @popular.category_list.add("linux", "dos")
    @popular.save
    @relevant.category_list.add("dos")
    @relevant.save
    @irelevant.category_list.add("linux")
    @irelevant.save

    assert Alert.not_categorized_as("dos").include? @irelevant
    assert_not Alert.not_categorized_as("dos").include? @popular
    assert_not Alert.not_categorized_as("dos").include? @relevant

    assert Alert.not_categorized_as(["dos", "linux"]).empty?
  end

  test "get similar alerts based on categories" do
    @popular.category_list.add("linux", "dos")
    @popular.save
    @relevant.category_list.add("dos")
    @relevant.save
    @irelevant.category_list.add("sql_injection", "linux")
    @irelevant.save

    assert @irelevant.find_related_categories.include? @popular
    assert_not @irelevant.find_related_categories.include? @relevant

    assert @relevant.find_related_categories.include? @popular
    assert_not @relevant.find_related_categories.include? @irelevant

    assert @popular.find_related_categories.include? @irelevant
    assert @popular.find_related_categories.include? @relevant
  end

  test "get the 2 most popular categories" do
    @popular.category_list.add("linux", "dos")
    @popular.save
    @relevant.category_list.add("dos")
    @relevant.save
    @irelevant.category_list.add("windows", "linux")
    @irelevant.save
    another = Alert.create!
    another.category_list.add("sql_injection", "linux")
    another.save

    assert_equal "linux", Alert.most_popular_categories(2).first.name
    assert_equal "dos", Alert.most_popular_categories(2).second.name
  end

  test "a user likes alerts" do
    @popular.liked_by(@user)
    @relevant.liked_by(@user)

    assert_equal 2, @user.votes.size
    assert_equal 1, @popular.likes.size
    assert_equal 1, @relevant.likes.size
  end

  test "a user unlikes liked alerts" do
    @popular.liked_by(@user)

    assert_equal 1, @user.votes.size
    assert_equal 1, @popular.likes.size

    @popular.unliked_by(@user)

    assert_equal 0, @user.votes.size
    assert_equal 0, @popular.likes.size
  end

  test "a user dislikes alerts" do
    @popular.disliked_by(@user)
    @relevant.liked_by(@user)

    assert_equal 2, @user.votes.size
    assert_equal 1, @user.votes.up.size
    assert_equal 1, @user.votes.down.size
    assert_equal 1, @popular.dislikes.size
  end

  test "a user undislikes disliked alerts" do
    @popular.disliked_by(@user)

    assert_equal 1, @user.votes.size
    assert_equal 1, @popular.dislikes.size

    @popular.undisliked_by(@user)

    assert_equal 0, @user.votes.size
    assert_equal 0, @popular.dislikes.size
  end

  test "positive rates for alert" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    @popular.liked_by(joao)
    @popular.liked_by(maria)
    @popular.liked_by(pedro)

    assert_equal 3, @popular.rate
  end

  test "negative rates for alert" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    @popular.disliked_by(joao)
    assert_equal -2, @popular.rate

    @popular.disliked_by(maria)
    assert_equal -4, @popular.rate

    @popular.liked_by(pedro)
    assert_equal -3, @popular.rate
  end

  test "get avarage votes size" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    assert_equal 0, Alert.avarage_votes_size

    @popular.disliked_by(joao)
    @popular.liked_by(maria)
    # -1

    @relevant.liked_by(joao)
    @relevant.disliked_by(pedro)
    @relevant.liked_by(maria)
    # 0

    @irelevant.liked_by(pedro)
    @irelevant.liked_by(maria)
    # 2

    assert_in_delta 2.33, Alert.avarage_votes_size, 0.01
  end

  test "get avarage ratings" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    assert_equal 0, Alert.avarage_rate

    @popular.disliked_by(joao)
    @popular.liked_by(maria)
    # -1

    @relevant.liked_by(joao)
    @relevant.disliked_by(pedro)
    @relevant.liked_by(maria)
    # 0

    @irelevant.liked_by(pedro)
    @irelevant.liked_by(maria)
    # 2

    assert_in_delta 0.33, Alert.avarage_rate, 0.01
  end

  test "properly rankings of alerts" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    @popular.disliked_by(joao)
    @popular.liked_by(maria)
    # -1

    @relevant.liked_by(joao)
    @relevant.disliked_by(pedro)
    @relevant.liked_by(maria)
    # 0

    @irelevant.liked_by(pedro)
    @irelevant.liked_by(maria)
    # 2

    assert @irelevant.compute_ranking_score > @relevant.compute_ranking_score
    assert @relevant.compute_ranking_score > @popular.compute_ranking_score
    assert @irelevant.compute_ranking_score > @popular.compute_ranking_score
  end

  test "get rankings liked alerts" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    @popular.liked_by(joao)
    @popular.liked_by(pedro)
    @popular.liked_by(maria)

    @relevant.liked_by(joao)
    @relevant.liked_by(maria)

    @irelevant.liked_by(pedro)

    assert @popular.compute_ranking_score > @relevant.compute_ranking_score
    assert @relevant.compute_ranking_score > @irelevant.compute_ranking_score
  end

  test "get top rankings of alerts" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    joao.disliked(@popular)
    maria.liked(@popular)
    # -1

    joao.liked(@relevant)
    maria.liked(@relevant)
    pedro.disliked(@relevant)
    # 0

    maria.liked(@irelevant)
    pedro.liked(@irelevant)
    # 2

    top = Alert.top_ranked

    assert_equal @irelevant, top.first
    assert_equal @popular, top.last
  end

  test "get top rankings of alerts a user did not vote" do
    joao = users(:joao)
    maria = users(:maria)
    pedro = users(:pedro)

    joao.disliked(@popular)
    maria.liked(@popular)
    # -1

    joao.liked(@relevant)
    maria.liked(@relevant)
    pedro.disliked(@relevant)
    # 0

    maria.liked(@irelevant)
    # 1

    top = Alert.top_ranked_to(joao)

    assert top.include? @irelevant
    assert_not top.include? @relevant
    assert_not top.include? @popular

    top = Alert.top_ranked_to(maria)

    assert_not top.include? @irelevant
    assert_not top.include? @relevant
    assert_not top.include? @popular

    top = Alert.top_ranked_to(pedro)

    assert top.include? @irelevant
    assert_not top.include? @relevant
    assert top.include? @popular

    ranked_to = Alert.top_ranked_to
    general_ranking = Alert.top_ranked

    assert_equal ranked_to, general_ranking
  end

  test "create a content-based score for alert" do
    joao = users(:joao)

    @popular.content_based_scores.create(:user => joao, :score => 1.0)

    assert_equal joao, @popular.content_based_scores.first.user
    assert_equal 1.0, @popular.content_based_scores.first.score
    assert_equal 1, @popular.content_based_scores.count
    assert_equal 1, joao.content_based_scores.count
    assert_equal 1, ContentBasedScore.count
  end
end
