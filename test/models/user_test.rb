require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "a new user has no interest by default" do
    user = users(:joao)

    assert user.interest_list.empty?
  end

  test "add new interests to user" do
    user = users(:joao)
    user.interest_list.add("linux", "dos")

    user.save

    assert_not user.interest_list.empty?
    assert user.interest_list.include?("dos")
  end

  test "get the most and least popular interests" do
    joao = users(:joao)
    joao.interest_list.add("linux", "dos")
    joao.save
    maria = users(:maria)
    maria.interest_list.add("dos")
    maria.save

    assert_equal "dos", ActsAsTaggableOn::Tag.most_used.first.name
    assert_equal "linux", ActsAsTaggableOn::Tag.least_used.first.name
  end

  test "get users interested in Linux and Dos" do
    joao = users(:joao)
    joao.interest_list.add("linux", "dos")
    joao.save
    maria = users(:maria)
    maria.interest_list.add("dos")
    maria.save
    pedro = users(:pedro)
    pedro.interest_list.add("dos", "linux")
    pedro.save

    assert User.interested_in_all(["dos", "linux"]).include? joao
    assert User.interested_in_all(["dos", "linux"]).include? pedro
    assert_not User.interested_in_all(["dos", "linux"]).include? maria
  end

  test "get users interested in Linux OR Dos" do
    joao = users(:joao)
    joao.interest_list.add("linux", "dos")
    joao.save
    maria = users(:maria)
    maria.interest_list.add("dos")
    maria.save
    pedro = users(:pedro)
    pedro.interest_list.add("linux")
    pedro.save

    assert User.interested_in_at_least_one(["dos", "linux"]).include? joao
    assert User.interested_in_at_least_one(["dos", "linux"]).include? pedro
    assert User.interested_in_at_least_one(["dos", "linux"]).include? maria
  end

  test "get users not interested in neither Linux or Dos" do
    joao = users(:joao)
    joao.interest_list.add("linux", "dos")
    joao.save
    maria = users(:maria)
    maria.interest_list.add("dos")
    maria.save
    pedro = users(:pedro)
    pedro.interest_list.add("linux")
    pedro.save
    ana = users(:ana)
    ana.interest_list.add("dos", "linux")
    ana.save

    assert User.not_interested_in("dos").include? pedro
    assert_not User.not_interested_in("dos").include? joao
    assert_not User.not_interested_in("dos").include? maria

    assert User.not_interested_in(["dos", "linux"]).empty?
  end

  test "get similar user based on interests" do
    joao = users(:joao)
    joao.interest_list.add("linux", "dos")
    joao.save
    maria = users(:maria)
    maria.interest_list.add("dos")
    maria.save
    pedro = users(:pedro)
    pedro.interest_list.add("sql_injection", "linux")
    pedro.save

    assert pedro.find_related_interests.include? joao
    assert_not pedro.find_related_interests.include? maria

    assert maria.find_related_interests.include? joao
    assert_not maria.find_related_interests.include? pedro

    assert joao.find_related_interests.include? pedro
    assert joao.find_related_interests.include? maria
  end

  test "get the 2 most popular interests" do
    joao = users(:joao)
    joao.interest_list.add("linux", "dos")
    joao.save
    maria = users(:maria)
    maria.interest_list.add("dos")
    maria.save
    pedro = users(:pedro)
    pedro.interest_list.add("windows", "linux")
    pedro.save
    ana = users(:ana)
    ana.interest_list.add("sql_injection", "linux")
    ana.save

    assert_equal "linux", User.most_popular_interests(2).first.name
    assert_equal "dos", User.most_popular_interests(2).second.name
  end

  test "update the weight of users interests" do
    joao = users(:joao)
    joao.add_interests("linux", "dos")
    joao.add_interests("linux")

    assert_equal 2, joao.tag_weight("linux")
    assert_equal 1, joao.tag_weight("dos")
    assert_equal 0, joao.tag_weight("malware")

    joao.add_interests("linux", "dos", "malware")
    assert_equal 3, joao.tag_weight("linux")
    assert_equal 2, joao.tag_weight("dos")
    assert_equal 1, joao.tag_weight("malware")
  end

  test "a use likes some alerts" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)
    relevant = tweets(:relevant).becomes(Alert)
    irelevant = tweets(:irelevant).becomes(Alert)

    joao.liked popular
    joao.disliked irelevant

    assert joao.liked? popular
    assert joao.disliked? irelevant
    assert_not joao.liked? relevant
    assert_not joao.disliked? relevant
    assert_not joao.voted_for? relevant
  end

  test "get the total sum of the tag weights" do
    joao = users(:joao)
    joao.add_interests("linux", "dos")
    joao.add_interests("linux")

    assert_equal 3, joao.total_tag_weight

    joao.add_interests("linux", "dos", "malware")
    assert_equal 6, joao.total_tag_weight


  end

  test "get all items liked by an user" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)
    relevant = tweets(:relevant).becomes(Alert)
    irelevant = tweets(:irelevant).becomes(Alert)

    joao.liked popular
    joao.liked irelevant

    assert joao.find_liked_items.include? popular
    assert joao.find_liked_items.include? irelevant
    assert_not joao.find_liked_items.include? relevant
  end

  test "get all items unliked by an user" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)
    relevant = tweets(:relevant).becomes(Alert)
    irelevant = tweets(:irelevant).becomes(Alert)

    joao.disliked popular
    joao.disliked irelevant

    assert joao.find_disliked_items.include? popular
    assert joao.find_disliked_items.include? irelevant
    assert_not joao.find_disliked_items.include? relevant
  end

  test "get all items voted by an user" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)
    relevant = tweets(:relevant).becomes(Alert)
    irelevant = tweets(:irelevant).becomes(Alert)

    joao.liked popular
    joao.disliked irelevant

    assert joao.find_voted_items.include? popular
    assert joao.find_voted_items.include? irelevant
    assert_not joao.find_voted_items.include? relevant
  end

  test "add new interests when a user likes an item" do
    joao = users(:joao)
    joao.add_interests("linux", "dos")

    popular = tweets(:popular).becomes(Alert)
    popular.category_list.add("linux", "malware")
    popular.save

    joao.liked popular

    assert joao.interest_list.include?("malware")
  end

  test "add update interests weight when a user likes an item" do
    joao = users(:joao)
    joao.add_interests("linux", "dos")

    popular = tweets(:popular).becomes(Alert)
    popular.category_list.add("malware", "linux")
    popular.save
    relevant = tweets(:relevant).becomes(Alert)
    relevant.category_list.add("dos", "another")
    relevant.save

    joao.liked popular

    assert_equal 1, joao.tag_weight("malware")
    assert_equal 0, joao.tag_weight("another")
    assert_equal 2, joao.tag_weight("linux")
    assert_equal 1, joao.tag_weight("dos")

    joao.liked relevant

    assert_equal 1, joao.tag_weight("malware")
    assert_equal 1, joao.tag_weight("another")
    assert_equal 2, joao.tag_weight("linux")
    assert_equal 2, joao.tag_weight("dos")
  end

  test "clean recommendations" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)

    assert_difference('joao.content_based_scores.count', 1) do
      joao.content_based_scores.create(:alert => popular, :score => 1.0)
    end

    assert_difference('joao.collaborative_filtering_scores.count', 1) do
      joao.collaborative_filtering_scores.create(:alert => popular, :score => 1.0)
    end

    assert_difference('joao.collaborative_filtering_scores.count', -1) do
      assert_difference('joao.content_based_scores.count', -1) do
        joao.clean_recommendations(popular)
      end
    end
  end

  test "clean recommendations after a user like or dislike an alert" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)
    relevant = tweets(:relevant).becomes(Alert)

    joao.content_based_scores.create(:alert => popular, :score => 1.0)
    joao.content_based_scores.create(:alert => relevant, :score => 2.0)

    joao.collaborative_filtering_scores.create(:alert => popular, :score => 1.0)
    joao.collaborative_filtering_scores.create(:alert => relevant, :score => 2.0)

    assert_difference('joao.content_based_scores.count', -1) do
      assert_difference('joao.collaborative_filtering_scores.count', -1) do
        joao.liked popular
      end
    end

    assert_difference('joao.content_based_scores.count', -1) do
      assert_difference('joao.collaborative_filtering_scores.count', -1) do
        joao.disliked relevant
      end
    end
  end

  test "get recommendable alerts" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)
    relevant = tweets(:relevant).becomes(Alert)

    assert joao.get_recommendable_alerts.include? popular
    assert joao.get_recommendable_alerts.include? relevant

    joao.liked popular
    assert_not joao.get_recommendable_alerts.include? popular
    assert joao.get_recommendable_alerts.include? relevant

    joao.disliked relevant
    assert_not joao.get_recommendable_alerts.include? popular
    assert_not joao.get_recommendable_alerts.include? relevant
  end
end
