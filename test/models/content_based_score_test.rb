require 'test_helper'

class ContentBasedScoreTest < ActiveSupport::TestCase

  test "create a content-based score for user" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)

    joao.content_based_scores.create(:alert => popular, :score => 1.0)

    assert_equal popular, joao.content_based_scores.first.alert
    assert_equal 1.0, joao.content_based_scores.first.score
    assert_equal 1, joao.content_based_scores.count
    assert_equal 1, popular.content_based_scores.count
    assert_equal 1, ContentBasedScore.count
  end

  test "not allow the creation of an content-based score for existing user-alert relation" do
    joao = users(:joao)
    popular = tweets(:popular).becomes(Alert)

    joao.content_based_scores.create(:alert => popular, :score => 1.0)
    joao.content_based_scores.create(:alert => popular, :score => 1.0)

    assert_equal 1, joao.content_based_scores.count
    assert_equal 1, popular.content_based_scores.count
    assert_equal 1, ContentBasedScore.count

    assert_raises(ActiveRecord::RecordInvalid){ joao.content_based_scores.create!(:alert => popular, :score => 1.0) }
  end
end
