require 'test_helper'

class RecommenderHelperTest < ActiveSupport::TestCase
  setup do
    @joao = users(:joao)

    @popular = tweets(:popular).becomes(Alert)
    stub(@popular).compute_ranking_score { 3.0 }
    @popular.save
    @relevant = tweets(:relevant).becomes(Alert)
    stub(@relevant).compute_ranking_score { 2.0 }
    @relevant.save
    @irelevant = tweets(:irelevant).becomes(Alert)
    stub(@irelevant).compute_ranking_score { 1.0 }
    @irelevant.save

    @recommendable = Alert.new
    stub(@recommendable).compute_ranking_score { 2.5 }
    @recommendable.save!
  end

  test "add top ranked ids to recommendation hash" do
    general_ranked_alerts = Alert.top_ranked_to(@joao)
    recommendation = {}

    RecommenderHelper.update_recommendation_scores(general_ranked_alerts, recommendation)

    general_ranked_alerts.each do |alert|
      assert recommendation.has_key?(alert.id)
      assert_equal alert.ranking_score, recommendation[alert.id]
    end
  end

  test "compute the recommendation score" do
    stub(CollaborativeFilteringScore).recommend(@joao, 10) { [@recommendable, @irelevant] }
    stub(ContentBasedScore).recommend(@joao, 10) { [@popular, @relevant, @recommendable] }

    alerts = RecommenderHelper.recommended_list(@joao)

    assert_equal @recommendable, alerts[0]
    assert_equal @popular, alerts[1]
    assert_equal @relevant, alerts[2]
    assert_equal @irelevant, alerts[3]
  end
end
