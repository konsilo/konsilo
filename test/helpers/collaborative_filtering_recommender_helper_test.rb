require 'test_helper'

class CollaborativeFilteringRecommenderHelperTest < ActiveSupport::TestCase
  setup do
    @joao = users(:joao)
    @joao.extend CollaborativeFilteringRecommenderHelper

    @maria = users(:maria)
    @pedro = users(:pedro)

    Alert.destroy_all

    10.times {|i| Alert.create!}

    Alert.limit(5).order(:id).each do |alert|
      @joao.liked alert
    end
  end

  test "controlled number of articles" do
    assert_equal 10, Alert.count
  end

  test "a user extends the helper" do
    assert @joao.respond_to? :compute_nearest_neighbors
    assert @joao.respond_to? :get_high_collaborative_filtering_scores
  end

  test "properly compute users similarity" do
    Alert.limit(8).order("id desc").each do |alert|
      @maria.liked alert
    end

    @joao.compute_nearest_neighbors

    score = RelatedUser.find_by(first_user_id: @joao.id, second_user_id: @maria.id).score
    assert_in_delta 0.3, score, 0.01

    score = RelatedUser.find_by(first_user_id: @joao.id, second_user_id: @pedro.id).score
    assert_in_delta 0.0, score, 0.01
  end

  test "get high collaborative filtering scores for user" do
    Alert.limit(8).order("id desc").each do |alert|
      @maria.liked alert
    end

    @pedro.liked Alert.first
    @pedro.liked Alert.last

    joao_scores = @joao.get_high_collaborative_filtering_scores(5)

    @pedro.extend CollaborativeFilteringRecommenderHelper

    pedro_scores = @pedro.get_high_collaborative_filtering_scores(5)

    assert_not_equal joao_scores.first.alert_id, pedro_scores.first.alert_id
  end
end
