require 'test_helper'

class ContentBasedRecommenderHelperTest < ActiveSupport::TestCase
  setup do
    @joao = users(:joao)
    @joao.extend ContentBasedRecommenderHelper

    @popular = tweets(:popular).becomes(Alert)
    @popular.category_list.add("linux", "dos", "c++")
    @popular.save
    @relevant = tweets(:relevant).becomes(Alert)
    @relevant.category_list.add("linux", "java", "c++")
    @relevant.save
    @irelevant = tweets(:irelevant).becomes(Alert)
    @irelevant.category_list.add("c++", "dos", "malware")
    @irelevant.save

    @recommendable = Alert.create
  end

  test "a user extends the helper" do
    assert @joao.respond_to? :compute_tag_weight
    assert @joao.respond_to? :get_high_content_based_scores
  end

  test "properly compute the tag weight related to an recomendable article" do
    @recommendable.category_list.add("linux", "java", "another")
    @recommendable.save

    @joao.liked @popular
    @joao.liked @relevant
    @joao.liked @irelevant
    # linux = 2, dos = 2, c++ = 2, malware = 1, buffer = 1, java = 1

    assert_in_delta 0.33, @joao.compute_tag_weight(@recommendable), 0.01
  end

  test "properly compute the tag weight related for no categorized alert" do
    @joao.liked @popular
    @joao.liked @relevant
    @joao.liked @irelevant
    # linux = 3, dos = 1, c++ = 2, malware = 1, buffer = 1, java = 1

    assert_in_delta 0.0, @joao.compute_tag_weight(@recommendable), 0.01
  end

  test "properly compute the tag weight related for an user with no interests" do
    assert_in_delta 0.0, @joao.compute_tag_weight(@recommendable), 0.01
  end

  test "properly recommend alerts using content-based approach" do
    @joao.add_interests("linux", "dos", "java")
    @joao.liked @popular
    # linux: 2, dos: 2, c++: 1, java: 1 = 6

    high_scores = @joao.get_high_content_based_scores
    alerts_ids = high_scores.collect(&:alert_id)

    assert alerts_ids.include? @relevant.id
    assert alerts_ids.include? @irelevant.id
    assert_not alerts_ids.include? @popular.id

    assert_equal @relevant.id, high_scores.first.alert_id
    assert_equal @irelevant.id, high_scores.second.alert_id
  end

  test "update recommend scores using content-based approach" do
    @joao.add_interests("linux", "dos", "java")

    @joao.get_high_content_based_scores
    first_value = ContentBasedScore.find_by(user_id: @joao.id, alert_id: @relevant.id).score

    @joao.liked @popular
    @joao.liked @irelevant

    @joao.get_high_content_based_scores

    second_value = ContentBasedScore.find_by(user_id: @joao.id, alert_id: @relevant.id).score

    assert_not_equal first_value, second_value
  end
end
