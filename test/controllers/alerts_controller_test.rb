require 'test_helper'

class AlertsControllerTest < ActionController::TestCase
  setup do
    @alert = tweets(:popular)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:alerts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create alert" do
    assert_difference('Alert.count') do
      post :create, alert: { body: @alert.body, impact: @alert.impact, source: @alert.source, title: @alert.title, views: @alert.views }
    end

    assert_redirected_to alert_path(assigns(:alert))
  end

  test "should show alert" do
    get :show, id: @alert
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @alert
    assert_response :success
  end

  test "should update alert" do
    patch :update, id: @alert, alert: { body: @alert.body, impact: @alert.impact, source: @alert.source, title: @alert.title, views: @alert.views }
    assert_redirected_to alert_path(assigns(:alert))
  end

  test "should destroy alert" do
    assert_difference('Alert.count', -1) do
      delete :destroy, id: @alert
    end

    assert_redirected_to alerts_path
  end

  test "should an user like an unvoted alert" do
    joao = users(:joao)
    sign_in joao

    assert_difference('@alert.likes.size', 1) do
      put :like, :format => 'js', id: @alert
    end
  end

  test "should an user dislike an unvoted alert" do
    joao = users(:joao)
    sign_in joao

    assert_difference('@alert.dislikes.size', 1) do
      put :dislike, :format => 'js', id: @alert
    end
  end

  test "should an user not like an voted alert" do
    joao = users(:joao)
    joao.likes @alert
    sign_in joao

    assert_no_difference('@alert.likes.size') do
      put :like, :format => 'js', id: @alert
    end
  end

  test "should an user not dislike an voted alert" do
    joao = users(:joao)
    joao.dislikes @alert
    sign_in joao

    assert_no_difference('@alert.dislikes.size') do
      put :dislike, :format => 'js', id: @alert
    end
  end

  test "should an user get alert categories as interests" do
    joao = users(:joao)
    joao.add_interests("linux", "dos")
    sign_in joao

    @alert.category_list.add("malware", "linux")
    @alert.save

    malware_weight = joao.tag_weight("malware")
    linux_weight = joao.tag_weight("linux")
    dos_weight = joao.tag_weight("dos")

    put :like, :format => 'js', id: @alert

    assert_equal malware_weight + 1, joao.tag_weight("malware")
    assert_equal linux_weight + 1, joao.tag_weight("linux")
    assert_equal dos_weight, joao.tag_weight("dos")
  end
end
