require 'rubygems'
require 'httparty'

class GtewsClient
  include HTTParty
  base_uri 'gtews.cm.utfpr.edu.br/wst/service/gtews/alerts/'

  def self.last_day
    get("/lastDay").gsub(/\A\w+\(|\)\Z/, '')
  end

  def self.last_days
    get("/lastDays").gsub(/\A\w+\(|\)\Z/, '')
  end

  def self.last_week
    get("/lastWeek").gsub(/\A\w+\(|\)\Z/, '')
  end

  def self.last_month
    get("/lastMonth").gsub(/\A\w+\(|\)\Z/, '')
  end
end