class Alert < ActiveRecord::Base
  IMPACT = %w[
    High
    Medium
    Low
  ]

  acts_as_taggable_on :categories
  acts_as_votable

  has_many :content_based_scores
  has_many :users, :through => :content_based_scores

  has_many :collaborative_filtering_scores
  has_many :users, :through => :collaborative_filtering_scores

  after_validation :update_ranking_score

  def votes
    self.votes_for
  end

  def likes
    self.get_likes
  end

  def dislikes
    self.get_dislikes
  end

  def rate
    self.likes.size - 2 * self.dislikes.size
  end

  def compute_ranking_score
    avarage_votes = Alert.avarage_votes_size
    avarage_rate = Alert.avarage_rate

    divisor = avarage_votes + self.rate.abs

    return 0 if(divisor == 0 || self.votes.size == 0)

    ((avarage_votes*avarage_rate) + (self.votes.size*self.rate))/(divisor).to_f
  end

  def self.top_ranked(n = 10)
    Alert.limit(n).order("ranking_score desc")
  end

  def self.top_ranked_to(user = nil, n = 10)
    return Alert.top_ranked(n) unless user
    user.get_recommendable_alerts.limit(n).order("ranking_score desc")
  end

  def self.avarage_rate
    alerts = Alert.count
    rates = 0

    Alert.all.each do |alert|
      rates = rates + alert.rate
    end

    rates/alerts.to_f
  end

  def self.avarage_votes_size
    alerts = Alert.count
    return 0 if alerts == 0

    votes = ActsAsVotable::Vote.where(votable_type: "Alert").count

    votes/alerts.to_f
  end

  def self.categorized_as_all(categories)
    Alert.tagged_with(categories, :match_all => true)
  end

  def self.categorized_as_at_least_one(categories)
    Alert.tagged_with(categories, :any => true)
  end

  def self.not_categorized_as(categories)
    Alert.tagged_with(categories, :exclude => true)
  end

  def self.most_popular_categories(limit = 3)
    ActsAsTaggableOn::Tag.most_used(limit)
  end

  private

  def update_ranking_score
    self.ranking_score = self.compute_ranking_score
  end
end
