class CollaborativeFilteringScore < ActiveRecord::Base
  belongs_to :user
  belongs_to :alert
  validates :user_id, :uniqueness => { :scope => :alert_id }

  def self.recommend(user, n = 10)
    user.extend CollaborativeFilteringRecommenderHelper
    scores = user.get_high_collaborative_filtering_scores(n)

    alerts = []

    scores.each do |score|
      alerts << Alert.find(score.alert_id)
    end

    alerts
  end
end