class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_taggable_on :interests
  acts_as_voter

  has_many :content_based_scores
  has_many :alerts, :through => :content_based_scores

  has_many :collaborative_filtering_scores
  has_many :alerts, :through => :collaborative_filtering_scores

  has_many :first_related_users, :class_name => "RelatedUser", :foreign_key => "first_user_id", dependent: :destroy
  has_many :second_related_users, :class_name => "RelatedUser", :foreign_key => "second_user_id", dependent: :destroy

  def add_interests(*args)
    self.interest_list.add args
    self.save!

    self.update_weights(*args)
  end

  def add_alert_categories_as_interests(alert)
    self.add_interests(alert.category_list) unless alert.category_list.blank?
  end

  def tag_weight(name)
    tag = ActsAsTaggableOn::Tag.find_by_name(name)
    return 0 unless tag

    tagging = ActsAsTaggableOn::Tagging.references(self, tag).first
    return 0 unless tagging

    tagging.weight
  end

  def total_tag_weight
    self.taggings.sum("weight")
  end

  def update_weights(*args)
    args.flatten.each do |interest|
      tag = ActsAsTaggableOn::Tag.find_by_name(interest)
      tagging = ActsAsTaggableOn::Tagging.references(self, tag).first
      tagging.weight = tagging.weight + 1
      tagging.save!
    end
  end

  def liked(alert)
    self.likes(alert)
    alert.save
    self.add_alert_categories_as_interests(alert)
    self.clean_recommendations(alert)
  end

  def disliked(alert)
    self.dislikes(alert)
    alert.save
    self.clean_recommendations(alert)
  end

  def clean_recommendations(alert)
    content_based_score = ContentBasedScore.find_by(user_id: self.id, alert_id: alert.id)
    content_based_score.destroy if content_based_score

    collaborative_filtering_score = CollaborativeFilteringScore.find_by(user_id: self.id, alert_id: alert.id)

    collaborative_filtering_score.destroy if collaborative_filtering_score
  end

  def get_recommendable_alerts
    voted_alerts_ids = self.get_voted(Alert).collect(&:id)

    voted_alerts_ids = -1 if voted_alerts_ids.blank?

    Alert.where('id NOT IN (?)', voted_alerts_ids)
  end

  def self.interested_in_all(interests)
    User.tagged_with(interests, :match_all => true)
  end

  def self.interested_in_at_least_one(interests)
    User.tagged_with(interests, :any => true)
  end

  def self.not_interested_in(interests)
    User.tagged_with(interests, :exclude => true)
  end

  def self.most_popular_interests(limit = 3)
    ActsAsTaggableOn::Tag.most_used(limit)
  end
end
