class ContentBasedScore < ActiveRecord::Base
  belongs_to :user
  belongs_to :alert
  validates :user_id, :uniqueness => { :scope => :alert_id }

  def self.recommend(user, n = 10)
    user.extend ContentBasedRecommenderHelper
    scores = user.get_high_content_based_scores(n)

    alerts = []

    scores.each do |score|
      alerts << Alert.find(score.alert_id)
    end

    alerts
  end
end