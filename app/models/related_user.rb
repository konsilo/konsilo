class RelatedUser < ActiveRecord::Base
  # TODO: handle duplications

  belongs_to :first_user, :class_name => "User"
  belongs_to :second_user, :class_name => "User"
  validates :first_user, :uniqueness => { :scope => :second_user }

  def other(user)
    return self.second_user if user == self.first_user
    return self.first_user if user == self.second_user

    nil
  end

  def self.get_most_similar_users(user, n = 5)
    # TODO: define inferior limit to score
    where('first_user_id = ? OR second_user_id = ?', user.id, user.id).limit(5).order("score desc")
  end
end