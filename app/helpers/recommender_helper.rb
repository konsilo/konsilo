module RecommenderHelper
  # TODO: Refactoring this module
  # TODO: make N a configuration variable

  GAMA = 2

  def self.recommended_list(user = nil, n = 10)
    general_ranked_alerts = Alert.top_ranked_to(user, n)

    return general_ranked_alerts unless user

    collaborative = CollaborativeFilteringScore.recommend(user, n)
    content_based = ContentBasedScore.recommend(user, n)

    recommendation = {}

    RecommenderHelper.update_recommendation_scores(general_ranked_alerts, recommendation)
    RecommenderHelper.update_recommendation_scores(collaborative, recommendation)
    RecommenderHelper.update_recommendation_scores(content_based, recommendation)

    recommendation = recommendation.sort_by {|id, score| score}.reverse

    alerts = []
    n.times do |i|
      break if i >= recommendation.size
      alerts << Alert.find(recommendation[i].first)
    end

    alerts
  end

  def self.update_recommendation_scores(recommendations_array, recommendation_hash)
    recommendations_array.each do |alert|
      if recommendation_hash.has_key?(alert.id)
        recommendation_hash[alert.id] = recommendation_hash[alert.id] * RecommenderHelper::GAMA
      else
        recommendation_hash[alert.id] = alert.ranking_score
      end
    end
  end
end