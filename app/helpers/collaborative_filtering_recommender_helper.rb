module CollaborativeFilteringRecommenderHelper
  # TODO: Refactoring this module

  def get_high_collaborative_filtering_scores(n = 10)
    self.compute_nearest_neighbors

    # TODO: Put a minimum value
    neighbors = RelatedUser.get_most_similar_users(self)

    alerts = self.get_recommendable_alerts

    alerts.each do |alert|
      collaborative_filtering_scores = CollaborativeFilteringScore.find_or_initialize_by(user_id: self.id, alert_id: alert.id)

      a = 0
      b = 0
      neighbors.each do |n|
        other_user = n.other(self)
        rating = other_user.liked?(alert) ? 1 : 0
        a = a + (rating * n.score)
        b = b + n.score
      end

      if b == 0
        score = 0
      else
        score = a/b.to_f
      end

      collaborative_filtering_scores.score = score
      collaborative_filtering_scores.save!
    end

    self.collaborative_filtering_scores.limit(n).order("score desc")
  end

  def compute_nearest_neighbors
    voted_alerts = self.get_up_voted(Alert)

    User.all.each do |user|
      next if self.id == user.id

      related_user = RelatedUser.find_by(first_user_id: user.id, second_user_id: self.id)
      related_user = RelatedUser.find_or_initialize_by(first_user_id: self.id, second_user_id: user.id) if related_user.blank?

      user_voted_alerts = user.get_up_voted(Alert)
      voted_intersection = voted_alerts & user_voted_alerts
      voted_union = voted_alerts | user_voted_alerts

      if voted_union.blank?
        related_user.score = 0
      else
        related_user.score = (voted_intersection.count).to_f/(voted_union.count)
      end

      related_user.save!
    end
  end
end