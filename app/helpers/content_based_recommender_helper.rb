module ContentBasedRecommenderHelper
  def get_high_content_based_scores(n = 10)
    alerts = self.get_recommendable_alerts

    alerts.each do |alert|
     content_based_scores = ContentBasedScore.find_or_initialize_by(user_id: self.id, alert_id: alert.id)
      score = self.compute_tag_weight(alert)
      content_based_scores.score = score
      content_based_scores.save!
    end

    self.content_based_scores.limit(n).order("score desc")
  end

  def compute_tag_weight(alert)
    return 0 if self.total_tag_weight == 0

    alert_tags = 0
    alert.category_list.each do |category|
      alert_tags = alert_tags + self.tag_weight(category)
    end

    alert_tags/self.total_tag_weight.to_f
  end
end