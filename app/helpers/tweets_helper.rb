module TweetsHelper

  def self.create_alerts(period = :last_day)
    return unless GtewsClient.respond_to? period

    response = TweetsHelper.clean_json(GtewsClient.send(period))
    alerts = JSON.parse(response)

    alerts.each do |alert|
      if Tweet.where(:group_id => alert["groupId"]).blank?
        TweetsHelper.create_alert alert
      end
    end
  end

  protected

  def self.create_alert alert_data
    tweet = Tweet.new
    tweet.body = alert_data["tweetTextMessage"]
    tweet.group_id = alert_data["groupId"]
    tweet.number_of_users = alert_data["numberOfUsersThatPostThisMessages"]
    tweet.number_of_days = alert_data["totalNumbersOfDays"]
    tweet.burst_number = alert_data["burstNumber"]
    tweet.first_date = Time.zone.parse(alert_data["firstDate"]).utc
    tweet.tweet_type = alert_data["type"]
    tweet.total_number = alert_data["numberOfTweetsThatComposeTheGroup"]
    tweet.last_date = Time.zone.parse(alert_data["lastDate"]).utc
    tweet.category_list.add(TweetsHelper.extract_categories(tweet.body))
    tweet.save
  end

  def self.clean_json response
    response.gsub(/\A\w+\(|\)\Z/, '') unless response.blank?
  end

  def self.extract_categories alert_body
    categories = alert_body.scan(/#\w+/).flatten
    categories.map! {|category| category.gsub("#", "")}
  end
end
