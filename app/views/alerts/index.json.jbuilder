json.array!(@alerts) do |alert|
  json.extract! alert, :id, :impact, :source, :title, :body, :views
  json.url alert_url(alert, format: :json)
end
