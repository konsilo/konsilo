class HomeController < ApplicationController
  def index
    @alerts = Alert.order('created_at DESC')
    @recommended_alerts = RecommenderHelper.recommended_list(current_user)
  end
end
