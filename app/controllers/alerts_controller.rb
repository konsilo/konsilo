class AlertsController < ApplicationController
  before_action :set_alert, only: [:show, :edit, :update, :destroy, :like, :dislike, :add_tag, :update_tags]

  respond_to :html

  def index
    @alerts = Alert.all
    respond_with(@alerts)
  end

  def like
    @status = :error

    unless current_user.voted_for? @alert
      current_user.liked @alert
      @status = :created
    end

    respond_to do |format|
      format.js { render layout: false, status: @status }
    end
  end

  def dislike
    @status = :error

    unless current_user.voted_for? @alert
      current_user.disliked @alert
      @status = :created
    end

    respond_to do |format|
      format.js { render layout: false,  status: @status }
    end
  end

  def update_tags

    @alert.update(alert_params)

    respond_to do |format|
      format.js { render layout: false,  status: @status }
    end
  end

  def add_tag
    respond_to do |format|
      format.js { render layout: false,  status: @status }
    end
  end

  def show
    respond_with(@alert)
  end

  def new
    @alert = Alert.new
    respond_with(@alert)
  end

  def edit
  end

  def create
    @alert = Alert.new(alert_params)
    @alert.save
    respond_with(@alert)
  end

  def update
    @alert.update(alert_params)
    respond_with(@alert)
  end

  def destroy
    @alert.destroy
    respond_with(@alert)
  end

  private
    def set_alert
      @alert = Alert.find(params[:id])
    end

    def alert_params
      params.require(:alert).permit(:impact, :source, :title, :body, :views, :category_list)
    end
end
