class RegistrationsController < Devise::RegistrationsController

  before_filter :set_most_popular_interests, :only => [:edit, :new]
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def set_most_popular_interests
    limit = 10
    @interests = User.most_popular_interests(10)
  end

  def configure_permitted_parameters
     devise_parameter_sanitizer.for(:sign_in){ |u| u.permit(:email, :password) }
     devise_parameter_sanitizer.for(:sign_up){ |u| u.permit(:name, :username, :about,  :email, :password, :password_confirmation, :interest_list)}
     devise_parameter_sanitizer.for(:account_update){ |u| u.permit(:name, :username, :about, :email, :password, :password_confirmation, :interest_list) }
   end
end