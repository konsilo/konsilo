# About Konsilo
Konsilo is a Recommender System to recommend cyber security alerts for Network Administrators that has been developed during a Computer Network Programming subject at University of São Paulo (USP).

* **Professor**: Daniel Batista
* **University**: [University of São Paulo (USP)](http://www.usp.br)
* **Institute**: [Institute of Mathematics and Statistics (IME)](http://www.ime.usp.br)

You can find the latest version and the source code in our git repository at https://gitlab.com/arthurmde/konsilo

# Setup Konsilo Environment

## Development

* Dependencies
  * Install nodejs: ``` sudo apt-get install nodejs ```
  * Install postgresql-9.3
  * Install postgresql-server-dev-9.3
  * Ruby Version: 1.2.3
  * Rails Version: 4.0.0
* Configuration

```
bundle install
cp config/database.yml.dist config/database.yml
rake db:create
rake db:setup
```
* Update crontab to import security alerts
```
whenever -i
```

* Run test suite

```
rake
```

# Authors

* Arthur de Moura Del Esposte <arthurmde at gmail dot com || esposte at ime dot usp dot br>

# License

Copyright © 2015 The Author developers.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see www.gnu.org/licenses/