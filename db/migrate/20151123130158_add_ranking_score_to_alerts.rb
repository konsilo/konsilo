class AddRankingScoreToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :ranking_score, :float, default: 0.0
  end
end
