class AddTweetFieldsToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :group_id, :string
    add_column :alerts, :number_of_users, :integer
    add_column :alerts, :number_of_days, :integer
    add_column :alerts, :burst_number, :integer
    add_column :alerts, :first_date, :datetime
    add_column :alerts, :last_date, :datetime
    add_column :alerts, :tweet_type, :integer
    add_column :alerts, :total_number, :integer
  end
end
