class CreateRelatedUsers < ActiveRecord::Migration
  def change
    create_table :related_users do |t|
      t.integer  :first_user_id
      t.integer  :second_user_id
      t.float :score, default: 0.0

      t.timestamps
    end
  end
end
