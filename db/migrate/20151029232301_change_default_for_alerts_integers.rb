class ChangeDefaultForAlertsIntegers < ActiveRecord::Migration
  def change
    change_column :alerts, :views, :integer, :default => 0
    change_column :alerts, :number_of_users, :integer, :default => 0
    change_column :alerts, :number_of_days, :integer, :default => 0
    change_column :alerts, :burst_number, :integer, :default => 0
    change_column :alerts, :total_number, :integer, :default => 0
  end
end
