class CreateAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.string :impact
      t.string :source
      t.string :title
      t.text :body
      t.integer :views

      t.timestamps
    end
  end
end
