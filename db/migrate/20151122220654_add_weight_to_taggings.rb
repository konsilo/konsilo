class AddWeightToTaggings < ActiveRecord::Migration
  def change
    add_column :taggings, :weight, :integer, default: 0
  end
end
