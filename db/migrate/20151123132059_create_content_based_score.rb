class CreateContentBasedScore < ActiveRecord::Migration
  def change
    create_table :content_based_scores do |t|
      t.belongs_to :user
      t.belongs_to :alert
      t.float :score, default: 0.0

      t.timestamps
    end
  end
end
