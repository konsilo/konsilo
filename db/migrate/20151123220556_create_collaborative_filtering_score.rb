class CreateCollaborativeFilteringScore < ActiveRecord::Migration
  def change
    create_table :collaborative_filtering_scores do |t|
      t.belongs_to :user
      t.belongs_to :alert
      t.float :score, default: 0.0

      t.timestamps
    end
  end
end
