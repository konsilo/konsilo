ActsAsTaggableOn::Tagging.class_eval do
  def self.references(taggable, tag)
    where(taggable_id: taggable.id, tag_id: tag.id)
  end
end